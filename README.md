# Guía de instalación #

## Pagos recurrentes Identifix ##

### Requerimientos previos: ###

Para añadir las funcionalidades de cargos recurrentes se requiere:

Un sitio web con:

- Wordpress 5.2+ https://wordpress.org/
- Template BeTheme https://themes.muffingroup.com/be/splash/
- Woocommerce https://woocommerce.com/

### Instrucciones: ###

1. Instalar como plugin en (comprimido en .zip) las carpetas incluidas
* loco-translate
* openpay-woosubscriptions-master
* woocommerce-subscriptions
* woocommerce-all-products-for-subscriptions

2. Agregar los archivos .mo y .po en la ruta del servidor: /wp-content/languages/plugins
3. Sustituir el archivo functions.php en /wp-content/themes/betheme-child
4. Agregar el directorio /wp-content/themes/betheme-child/woocommerce/cart y colocar en esta ruta el archivo cart.php


### Consideraciones generales ###

No desinstalar ningún plugin
Mantener los siguientes plugins en la versión indicada a continuación. 

woocommerce-subscriptions
Versión 2.5.7

woocommerce-all-products-for-subscriptions
Versión 2.3.2

En caso de que algún otro plugin afecte la funcionalidad de los pagos recurrentes, es posible que sea necesario hacer alguna actualización. 